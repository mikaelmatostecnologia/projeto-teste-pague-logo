<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Admin',
            'email' => 'paguelogo@paguelogo',
            'password' => bcrypt('secret'),
        ]);

        factory(User::class, 3)->create();
    }
}
