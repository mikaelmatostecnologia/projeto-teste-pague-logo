<div class="table-responsive">
    <table class="table table-borderless table-hover bg-white shadow-sm">
        {{ $slot }}
    </table>
</div>